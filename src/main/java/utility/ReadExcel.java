package utility;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		XSSFWorkbook wbook= new XSSFWorkbook("./exceldata/TC001.xlsx");
		XSSFSheet wsheet = wbook.getSheet("Sheet1");
		
		int rowcount = wsheet.getLastRowNum();
		System.out.println("Row Count:" + rowcount);
		int colcount = wsheet.getRow(0).getLastCellNum();
		System.out.println("Column Count:" + colcount);
		for (int i = 1; i <=rowcount; i++) {
			
			XSSFRow row = wsheet.getRow(i);
			for (int j = 0; j < colcount; j++) {
				
				XSSFCell cell = row.getCell(j);
				String text = cell.getStringCellValue();
				System.out.println(text);
				
			}
			
		}
		

	}

}
