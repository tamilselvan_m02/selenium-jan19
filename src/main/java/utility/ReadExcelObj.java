package utility;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelObj {
	
	static Object[][] data;

	public static Object[][] readdata(String datasheetname) {
		// TODO Auto-generated method stub

		XSSFWorkbook wbook;
		try {
			wbook = new XSSFWorkbook("./exceldata/" + datasheetname + ".xlsx");

			XSSFSheet wsheet = wbook.getSheet("Sheet1");

			int rowcount = wsheet.getLastRowNum();
			System.out.println("Row Count:" + rowcount);
			int colcount = wsheet.getRow(0).getLastCellNum();
			System.out.println("Column Count:" + colcount);
			data = new Object[rowcount][colcount];
			for (int i = 1; i <= rowcount; i++) {

				XSSFRow row = wsheet.getRow(i);
				for (int j = 0; j < colcount; j++) {

					XSSFCell cell = row.getCell(j);
					data[i - 1][j] = cell.getStringCellValue();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;

	}

}
