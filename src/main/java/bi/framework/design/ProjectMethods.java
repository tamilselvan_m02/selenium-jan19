package bi.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import bl.framework.api.SeleniumBase;
import utility.ReadExcelObj;

public class ProjectMethods extends SeleniumBase {
	
	public String datasheetname;
	
    @Parameters({"url","username","pwd"})
	@BeforeMethod
	//@BeforeMethod(groups="common")
	public void login(String url,String uname,String pwd) {
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, uname); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pwd); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
	}
    
    @DataProvider(name="fetchdata")
    public Object[][] getData()
    {
    	return ReadExcelObj.readdata(datasheetname);
    }

}
