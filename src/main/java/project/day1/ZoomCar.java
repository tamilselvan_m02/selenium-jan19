package project.day1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//launch browser
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		
		//loadurl
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		
		driver.findElementByClassName("search").click();
		driver.findElementByXPath("//div[contains(text(),'Kodambakkam')]").click();
		driver.findElementByXPath("//button[(text()='Next')]").click();
		
		// Get the current date
		
		Date date = new Date();
      //Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd");
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		System.out.println("Booking Date:"+ tomorrow);
		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		driver.findElementByXPath("//button[(text()='Next')]").click();
		driver.findElementByXPath("//button[(text()='Done')]").click();
		
		List<WebElement> total = driver.findElementsByXPath("//div[@class='car-listing']");
		
		int size = total.size();
		System.out.println("Number of Results:" +size);
		//driver.findElementByXPath("//div[(text()=' Price: High to Low ')]").click();
		
		//String brand = driver.findElementByXPath("//div[@class='car-item']//h3")
		//System.out.println(driver.findElementByXPath("//div[@class='car-item']//h3").getText());
		List<WebElement> pricelist = driver.findElementsByXPath("//div[@class='price']");
		List<Integer> pricesorted = new ArrayList<>(); 
		for (WebElement pricemax : pricelist) 
		{
			
			String newprice = pricemax.getText().replaceAll("[^0-9]", "");
			int maxprice = Integer.parseInt(newprice);
			pricesorted.add(maxprice);
			
		}
		Collections.sort(pricesorted);
		Integer finalprice = pricesorted.get(pricesorted.size()-1);
		System.out.println("Max price is" + finalprice);		
		
		String brand = driver.findElementByXPath("//div[contains(text(),'"+finalprice+"')]/preceding::h3[1]").getText();
		System.out.println("Brand Name is" + brand);
		driver.findElementByXPath("//div[contains(text(),'"+finalprice+"')]/following-sibling::button").click();
		
		
		

	}

}
