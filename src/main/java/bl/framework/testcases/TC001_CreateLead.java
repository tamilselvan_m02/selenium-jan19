package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;


public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "Gayatri";
		category = "Smoke";
		datasheetname ="TC001";
	}

	//@Test(groups="smoke")
	@Test(dataProvider="fetchdata")
	public void createLead(String cname, String fname,String lname) {
		//login();
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
		WebElement elecrmsfa = locateElement("linktext", "CRM/SFA");
		click(elecrmsfa);
		WebElement eleleads = locateElement("linktext", "Leads");
		click(eleleads);
		WebElement elecrelead = locateElement("linktext", "Create Lead");
		click(elecrelead);
		WebElement elecomname = locateElement("id", "createLeadForm_companyName");
		clearAndType(elecomname, cname);
		WebElement elefirstname = locateElement("id", "createLeadForm_firstName");
		clearAndType(elefirstname, fname);
		WebElement elelastname = locateElement("id", "createLeadForm_lastName");
		clearAndType(elelastname, lname);
		WebElement elecreatlead = locateElement("name", "submitButton");
		click(elecreatlead);
		
		driver.close();
		
		}
	
	@DataProvider(name="getdata")
	public String[][] fetchData(){
		
		String[][] data= new String[2][3];
		
		data[0][0]="Infy";
		data[0][1]="Tamil";
		data[0][2]="Mano";
		
		data[1][0]="Infy";
		data[1][1]="Ashwath";
		data[1][2]="Tamil";
		return data;
				
	}
	
	@DataProvider(name="getdata1")
	public String[][] fetchData1(){
		
		String[][] data= new String[2][3];
		
		data[0][0]="Infy";
		data[0][1]="Ravi";
		data[0][2]="Mano";
		
		data[1][0]="Infy";
		data[1][1]="Mithun";
		data[1][2]="Tamil";
		return data;
				
	}

	
}








