package bl.framework.testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport {
	
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	
	
	
	@Test
	public void myReport()
	{
		html= new ExtentHtmlReporter("./report/extentreport.html");
		extent= new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test = extent.createTest("TC001_Login", "Login to LeafTaps");
		test.assignAuthor("Tamil");
		test.assignCategory("Sanity");
		try {
			test.pass("Username Entered Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
			test.fail("Login Failed",MediaEntityBuilder.createScreenCaptureFromPath("./../snap/img2.png").build() );
			test.fail("Username Entered Successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		extent.flush();
		
		
	}
	

}
