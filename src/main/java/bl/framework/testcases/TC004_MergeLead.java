package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods {
	
	@Test(groups="sanity")
	public void mergeLead(){
		
	//login();
	//WebElement eleLogout = locateElement("class", "decorativeSubmit");
	//click(eleLogout);
	WebElement elecrmsfa = locateElement("linktext", "CRM/SFA");
	click(elecrmsfa);
	click(locateElement("linktext", "Leads"));
	WebElement elemerglead = locateElement("linktext", "Merge Leads");
	click(elemerglead);
	
	/*WebElement elecomname = locateElement("id", "createLeadForm_companyName");
	clearAndType(elecomname, "Infosys");
	WebElement elefirstname = locateElement("id", "createLeadForm_firstName");
	clearAndType(elefirstname, "Tamilselvan");
	WebElement elelastname = locateElement("id", "createLeadForm_lastName");
	clearAndType(elelastname, "Manoharan");
	WebElement elecreatlead = locateElement("name", "submitButton");
	click(elecreatlead);*/
	
	driver.close();
	
	}
	
}
